package stringcalculator;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringCalculator {
    private String delimiter = "[,\n]";
    private String numbers;

    public int add(String numbers) {

        parseNumbers(numbers);

        if (getNumbers().anyMatch(n -> n < 0)) {
            String negativeNumbers = getNumbers().filter(n -> n < 0)
                    .mapToObj(Integer::toString)
                    .collect(Collectors.joining(","));

            throw new IllegalArgumentException("negatives not allowed: " + negativeNumbers);
        }

        return getNumbers().sum();
    }

    private IntStream getNumbers() {
        if (numbers.isEmpty())
            return IntStream.empty();

        return Stream.of(numbers.split(delimiter)).mapToInt(Integer::parseInt);
    }

    private void parseNumbers(String numbers) {
        if (numbers.startsWith("//")) {
            String[] strArr = numbers.split("\n");
            delimiter = "[" + strArr[0].substring(2) + "]";
            this.numbers = strArr[1];
        } else {
            this.numbers = numbers;
        }
    }
}
