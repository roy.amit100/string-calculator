package stringcalculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {
    private StringCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    void addMethodShouldReturnZeroIfInputStringIsEmpty() {
        assertEquals(0, calculator.add(""));
    }

    @Test
    void shouldReturnTheNumberAsSumIfGivenSingleNumberAsInput() {
        assertEquals(1, calculator.add("1"));
        assertEquals(123, calculator.add("123"));
    }

    @Test
    void shouldReturnSumOfTwoNumbersSeparatedByComma() {
        assertEquals(3, calculator.add("1,2"));
        assertEquals(13, calculator.add("5,8"));
    }

    @Test
    void shouldReturnSumOfMoreThanTwoNumbersSeparatedByComma() {
        assertEquals(6, calculator.add("1,2,3"));
        assertEquals(22, calculator.add("4,5,6,7"));
    }

    @Test
    void returnSumOfNumbersDelimitedByNewLine() {
        assertEquals(3, calculator.add("1\n2"));
    }

    @Test
    void returnSumOfNumbersDelimitedByNewLineOrComma() {
        assertEquals(6, calculator.add("1\n2,3"));
        assertEquals(6, calculator.add("1,2\n3"));
    }

    @Test
    void returnSumSpecifiedByCustomDelimiter() {
        assertEquals(3, calculator.add("//;\n1;2"));
        assertEquals(3, calculator.add("//$\n1$2"));
        assertEquals(3, calculator.add("//#\n1#2"));
        assertEquals(3, calculator.add("//*\n1*2"));
    }

    @Test
    void shouldThrowExceptionForNegativeNumbers() {
        IllegalArgumentException exception;

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("1,-2"));
        assertEquals("negatives not allowed: " + "-2", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("1,-2,4,-5,11,-3"));
        assertEquals("negatives not allowed: " + "-2,-5,-3", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("-2\n-3"));
        assertEquals("negatives not allowed: " + "-2,-3", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("1\n-2"));
        assertEquals("negatives not allowed: " + "-2", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("//;\n-1;2"));
        assertEquals("negatives not allowed: " + "-1", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> calculator.add("//;\n-11;-2"));
        assertEquals("negatives not allowed: " + "-11,-2", exception.getMessage());
    }
}